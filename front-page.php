
<?php get_header(); ?>

<main class="structure">
	<div class="bg-content"></div>
	<section id="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 text-center">
					<div class="triangle">					
						<div class="content-triangle">
							<img class="d-none d-lg-inline" src="<?php echo get_template_directory_uri(). '/dist/images/triangle.png'; ?>" alt="">	
							<h1 class="text-triangle">Estratégias de marketing<br>
							digital para sua marca</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="content" class="hide">
			<div class="container pt-3">
				<div class="row">
					<!-- <div class="col-lg-12 content-text pt-3">
						<?php /*if ( have_posts() ) : while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					else:
						 */?>
						<p>Conteúdo principal aqui</p>
					<?php //endif; ?>
				</div> -->
				<div class="col-lg-12 pt-4 pb-4">
					<div class="sub-title pb-3">
						<span class="pb-5">Sobre nós</span>
						<?php 
						echo the_field('sobre_nos');
						?>
					</div>

					<div class="col-12 text-center">
						<div class="spacing"></div>
					</div>
				</div>
				<div class="col-lg-4 pb-4 item-topic">
					<h3 class="text-center text-lg-left">Porque existimos?</h3>
					<?php 
					echo the_field('missao');
					?>

				</div>
				<div class="col-lg-4 pb-4 item-topic">
					<h3 class="text-center text-lg-left">Para onde vamos?</h3>
					<?php 
					echo the_field('visao');
					?>
				</div>
				<div class="col-lg-4 pb-4 item-topic">
					<h3 class="text-center text-lg-left">Como chegaremos lá?</h3>
					<?php 
					echo the_field('valores');
					?>
				</div>
			</div>
		</div>
	</section>
	<section id="process" class="hide">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 pt-4">
					<div class="arrows">
						<div class="arrow-left bg-arrow float-left arrow-left-service">
							<i class="fas fa-chevron-left"></i>
						</div>
						<div class="arrow-right bg-arrow float-right arrow-right-service">
							<i class="fas fa-chevron-right"></i>
						</div>
					</div>
					<div class="title-process text-center pb-5 pt-3">
						<h4>Serviços</h4>
						<div class="spacing"></div>
					</div>
					<div class="slider-items">
						<div class="col-lg-12 item-process text-center m-lg-3">

							<i class="fas fa-funnel-dollar"></i>
							<h4>Inbound Marketing</h4>
							<p>Estratégias de atração de clientes através da construção de autoridade para a marca. Montamos planejamento estratégico com técnicas de marketing de conteúdo e automação.</p>
							<button class="btn-default"><a href="#">Saiba Mais</a></button>

						</div>

						<div class="col-lg-12 item-process text-center m-lg-3">

							<i class="fab fa-searchengin"></i>
							<h4>SEO</h4>
							<p>Estratégias de atração de clientes através da construção de autoridade para a marca. Montamos planejamento estratégico com técnicas de marketing de conteúdo e automação.</p>
							<button class="btn-default"><a href="#">Saiba Mais</a></button>
						</div>


						<div class="col-lg-12 item-process text-center m-lg-3">

							<i class="fas fa-link"></i>
							<h4>Links Patrocinados</h4>
							<p>Anuncie na internet em canais variados com o máximo de eficiência e o menor custo possível. Através de diversas estratégias montamos o seu anúncio com foco em retorno de investimento.</p>
							<button class="btn-default"><a href="#">Saiba Mais</a></button>
						</div>


						<div class="col-lg-12 item-process text-center m-lg-3">

							<i class="fas fa-business-time"></i>
							<h4>Business Analysis</h4>
							<p>Anuncie na internet em canais variados com o máximo de eficiência e o menor custo possível. Através de diversas estratégias montamos o seu anúncio com foco em retorno de investimento.</p>
							<button class="btn-default"><a href="#">Saiba Mais</a></button>
						</div>


						<div class="col-lg-12 item-process text-center m-lg-3">

							<i class="fas fa-code"></i>
							<h4>Criação e Desenvolvimento</h4>
							<p>Construção de blogs, landing pages e sites responsivos, e de alto desempenho. Nossa equipe é focada em colocar o seu projeto no ar com o menor tempo possível.</p>
							<button class="btn-default"><a href="#">Saiba Mais</a></button>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</section>

	<section id="inbound" class="hide">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center mb-lg-5 spacing-box">
					<div class="jumbotron vertical-center text-center justify-content-center box-inbound mt-5 mt-lg-0">
						<div class="content-box">
							<i class="far fa-lightbulb"></i>
							<h4 class="mb-4">Aumentamos o trafego do seu site através de estratégias de marketing digital focadas em SEO! 
							</h4>
							<div class="desc-box">
								<p>
									O <strong>Google</strong> é hoje uma das ferramentas mais importantes para o marketing de uma marca. Seu cliente tem <strong>necessidades</strong> que só a <strong>sua empresa</strong> pode suprir e não ser <strong>o resultado</strong> do que ele procura na internet é o que separa você dos seus <strong>objetivos.</strong>
								</p>

								<a href="#"><button class="btn-default">Fale com um consultor</button></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="clients" class="hide">
		<div class="container-fluid">

			<div class="col-12 text-center pt-4 pb-4">
				<h4>Clientes</h4>
				<div class="spacing"></div>
			</div>
			<div class="items-client">

				<?php 
				$obj = get_queried_object_id();
						//print_r($obj);
						//$obj = 50; 
				if(	have_rows('clientes', $obj) ):
					while(	have_rows('clientes', $obj) ):
						the_row();

						$imageclient = get_sub_field('imagem', $obj);
						$imagealt = get_sub_field('alt_imagem', $obj);
						$imagecrop = wp_get_attachment_image_src( $imageclient , 'foto-cliente');

						?>
						<div class="col-md-12 col-lg-12 text-center">
							<div class="item">
								<img class="img-fluid" src="<?php echo $imagecrop[0]; ?>" alt="<?php echo $imagealt; ?>">
							</div>
						</div>
						<?php
					endwhile;
				endif;
				?>
			</div>
		</div>
	</section>
	<section id="cases" class="hide">
		<div class="container pb-5">

			<div class="col-12 text-center pt-4 pb-4">
				<h4>Cases</h4>
				<div class="spacing"></div>
			</div>

			<div class="arrows">
				<div class="arrow-left bg-arrow float-left arrow-left-case">
					<i class="fas fa-chevron-left"></i>
				</div>
				<div class="arrow-right bg-arrow float-right arrow-right-case">
					<i class="fas fa-chevron-right"></i>
				</div>
			</div>

			<div class="cases">

				<?php 
				$obj = get_queried_object_id();
						//print_r($obj);
						//$obj = 50; 
				if(	have_rows('cases', $obj) ):
					while(	have_rows('cases', $obj) ):
						the_row();

						$imagecase = get_sub_field('imagem', $obj);
						$imagealt = get_sub_field('alt_imagem', $obj);
						$titlecase = get_sub_field('titulo', $obj);
						$casedesc = get_sub_field('descricao', $obj);
						$linkcase = get_sub_field('link', $obj);
						$imagecrop = wp_get_attachment_image_src( $imagecase , 'foto-case');
						?>

						<div class="case-item">
							<div class="row">
								<div class="col-12 col-lg-6">
									<img class="fit-image" src="<?php echo $imagecrop[0]; ?>" alt="<?php echo $imagealt; ?>">
								</div>
								<div class="col-12 col-lg-6 justify-content-center text-center">
									<div class="content-case pt-5">
										<p class="client-name"><?php echo $titlecase; ?></p>
										<p class="client-name"><?php echo $casedesc ?></p>

										<a href="<?php echo $linkcase; ?>">
											<button class="btn-default btn-case">Ler Case</button>
										</a>
									</div>
								</div>
							</div>
						</div>
						<?php
					endwhile;
				endif;
				?>
			</div>
		</div>
	</section>

	<section id="doubt" class="hide">
		<div class="bg-doubt"></div>
		<div class="container">
			<div class="row">

				<div class="col-12 col-lg-3 box-doubt text-center">
					<div class="content-counter">
						<p class="number"><span class="number-animate">170</span></p>
						<p>clientes atendidos</p>
					</div>
				</div>

				<div class="col-12 col-lg-3 box-doubt text-center">
					<div class="content-counter">
						<p class="number">+ de <span class="number-animate">3500</span></p>	
						<p>palavras-chave na 1ª página do Google</p>
					</div>
				</div>

				<div class="col-12 col-lg-3 box-doubt text-center">
					<div class="content-counter">
						<p class="number">+ de <span class="number-animate">20</span> mil</p>	
						<p>ligações rastreadas</p>

					</div>
				</div>

				<div class="col-12 col-lg-3 box-doubt text-center">
					<div class="content-counter">	
						<p class="number">+ de <span class="number-animate">50</span> mil</p>
						<p>conversões geradas</p>
					</div>
				</div>


			</div>
		</div>
	</section>
	<section id="depositions">
		<div class="container">
			<div class="main-title pt-4">
				<h4>Depoimentos</h4>
				<div class="spacing"></div>
			</div>
			
				<div class="col-l2 pt-4 pb-1">
					<div class="items-deposition">

					<div class="deposition-item">
						<p class="text-deposition">
							"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam."
						</p>
						<div class="author text-right">
							<p>Nome</p>
							<p>Função</p>
							<p>Empresa</p>	
						</div>
					</div>

					<div class="deposition-item">
						<p class="text-deposition">
							"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam."
						</p>
						<div class="author text-right">
							<p>Nome</p>
							<p>Função</p>
							<p>Empresa</p>	
						</div>
					</div>

					<div class="deposition-item">
						<p class="text-deposition">
							"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam."
						</p>
						<div class="author text-right">
							<p>Nome</p>
							<p>Função</p>
							<p>Empresa</p>	
						</div>
					</div>

					<div class="deposition-item">
						<p class="text-deposition">
							"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit at ducimus cum doloremque magnam nisi voluptate deserunt voluptates, voluptatem, unde quaerat, rerum accusamus, aliquam corporis recusandae pariatur veritatis. Blanditiis, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, voluptates blanditiis saepe inventore sit expedita incidunt dignissimos, veritatis provident reprehenderit suscipit veniam iusto quos ea, magni odit ipsum deserunt aperiam."
						</p>
						<div class="author text-right">
							<p>Nome</p>
							<p>Função</p>
							<p>Empresa</p>	
						</div>
					</div>

					</div>
				</div>
		
		</div>
	</section>
	<section id="blog">
		<div class="container">
			<div class="main-title pt-4">
				<h4>Últimas do Blog</h4>
				<div class="spacing"></div>
			</div>
			<div class="row pt-3">
				<div class="col-12 col-lg-4 pb-4">
					<div class="item-blog">
						<div class="content-item">
							<p>Vendas</p>
							<p class="data">##/##/####</p>
							<p class="title-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non magni eaque architecto minus voluptates omnis expedita ducimus voluptate.</p>
							<p class="desc-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio suscipit vero tempore ratione aperiam possimus vel a voluptates similique iure amet, quaerat officia molestias explicabo in deleniti illum. Cum, quis.</p>
							<p class="post-author"><i class="far fa-user"></i> post-author</p>
							<a href="">
								<button class="btn-postlink">Veja mais</button>
							</a>

						</div>
					</div>
				</div>
				<div class="col-12 col-lg-4 pb-4">
					<div class="item-blog">
						<div class="content-item">
							<p>Vendas</p>
							<p class="data">##/##/####</p>


							<p class="title-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non magni eaque architecto minus voluptates omnis expedita ducimus voluptate.</p>

							<p class="desc-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio suscipit vero tempore ratione aperiam possimus vel a voluptates similique iure amet, quaerat officia molestias explicabo in deleniti illum. Cum, quis.</p>

							<p class="post-author"><i class="far fa-user"></i> post-author</p>
							<a href="">
								<button class="btn-postlink">Veja mais</button>
							</a>

						</div>
					</div>
				</div>
				<div class="col-12 col-lg-4 pb-4">
					<div class="item-blog">
						<div class="content-item">
							<p>Vendas</p>
							<p class="data">##/##/####</p>

							<p class="title-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non magni eaque architecto minus voluptates omnis expedita ducimus voluptate.</p>

							<p class="desc-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio suscipit vero tempore ratione aperiam possimus vel a voluptates similique iure amet, quaerat officia molestias explicabo in deleniti illum. Cum, quis.</p>

							<p class="post-author"><i class="far fa-user"></i> post-author</p>

							<a href="">
								<button class="btn-postlink">Veja mais</button>
							</a>
						</div>
					</div>
				</div>
				<div class="col-12 text-lg-right text-center pt-2 pb-5">	
					<a href="#" class="blog-link">Veja todos os posts<i class="fas fa-reply-all"></i></a>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>