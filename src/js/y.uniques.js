// unique scripts go here.


function animateNumbers(){

	// console.log(scroll);
	// $(window).scroll(function () ){
		$('.number-animate').each(function () {


			$(this).prop('Counter',0).animate({
				Counter: $(this).text()
			}, {
				duration: 4000,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	}


	function fadeSections(){
		/* Every time the window is scrolled ... */
		$(window).scroll( function(){

			/* Check the location of each desired element */
			$('.hide').each( function(i){

				var bottom_of_object = $(this).offset().top + $(this).outerHeight();
				var bottom_of_window = $(window).scrollTop() + $(window).height();

				/* If the object is completely visible in the window, fade it it */
				if( bottom_of_window > bottom_of_object ){

					$(this).animate({'opacity':'1'},800);

				}

			}); 
			vScroll = $(document).scrollTop();
			console.log(vScroll);
			if(vScroll >= 2800){
				animateNumbers();
			}
		});
	} 

	function loadSlider(){
		$('.slider-items').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: true,
			prevArrow: $('.arrow-left-service'),
			nextArrow: $('.arrow-right-service'),
			responsive: [
			{
				breakpoint: 1024,
				settings:{
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
					fade: true
				}	
			}
			]
		});

		$('.items-client').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			dots: true,
			autoplay: true,
			prevArrow: null,
			nextArrow: null,
			responsive: [
			{
				breakpoint: 600,
				settings:{
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true, 
					fade: true,
					autoplay: true
				}
			}
			]
			
		});

		$('.items-team').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			dots: true,
			prevArrow: null,
			nextArrow: null,
			responsive: [
			{
				breakpoint: 600,
				settings:{
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true, 
					fade: true,
					autoplay: true
				}
			}]
			
		});

		$('.cases').slick({
			fade: true,
			prevArrow: $('.arrow-left-case'),
			nextArrow: $('.arrow-right-case'),
			responsive: [
			{
				breakpoint: 600,
				settings:{
					arrows: null,
					autoplay: true,
					infinite: true

				}
			}]
		});


		$('.items-deposition').slick({
			fade: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: false,
			autoplay: true,
			infinite: true
		});

	}

	function menuMobile(){

		$('.btn-menu').click(function(event) {
			$(this).toggleClass('active');
			$('.menu-opened').fadeIn();
			$('body, html').css('overflow-y', 'hidden');
			$('body, html').css('overflow-x', 'hidden');

			if( !$(this).hasClass('active') ){
				$('.menu-opened').fadeOut();
				$('body, html').css('overflow-y', 'initial');
				$('body, html').css('overflow-x', 'hidden');
			}
		});
	}

	function logoMenu(){
		$('.logo-menu').addClass('menu-init');
	}

	function animateTriangle(){

	// why it doesn't work on firefox?
	// Triângulo
	var card = $('.content-triangle img');

	$('.triangle').on('mousemove',function(e) {
		e.preventDefault();  
		var ax = -($(window).innerWidth()/2- e.pageX)/400;
		var ay = ($(window).innerHeight()/2- e.pageY)/400;
		// console.log('x:' + ax);
		// console.log('y:' + ay);
		card.attr("style", "transform: rotateY("+ax+"deg) rotateX("+ay+"deg);-webkit-transform: rotateY("+ax+"deg) rotateX("+ay+"deg);-moz-transform: rotateY("+ax+"deg) rotateX("+ay+"deg)");
	});

	$('.triangle').on('mouseout', function(e){
		card.attr("style", "transition: .2s ease; transform: rotateY("+0+"deg) rotateX("+0+"deg);-webkit-transform: rotateY("+0+"deg) rotateX("+0+"deg);-moz-transform: rotateY("+0+"deg) rotateX("+0+"deg)");

	});

	// itens Soluções
	var item1 = $('.content-image-1 img');
	$('.image-item-1').on('mousemove',function(e) {  
		e.preventDefault();
		var ax2 = -($(window).innerWidth()/2- e.pageX)/400;
		var ay2 = ($(window).innerHeight()/2- e.pageY)/400;
		item1.attr("style", "transform: rotateY("+ax2+"deg) rotateX("+ay2+"deg);-webkit-transform: rotateY("+ax2+"deg) rotateX("+ay2+"deg);-moz-transform: rotateY("+ax2+"deg) rotateX("+ay2+"deg)");
	});
	$('.image-item-1').on('mouseout', function(e){
		item1.attr("style", "transition: .2s ease; transform: rotateY("+0+"deg) rotateX("+0+"deg);-webkit-transform: rotateY("+0+"deg) rotateX("+0+"deg);-moz-transform: rotateY("+0+"deg) rotateX("+0+"deg)");

	});


	var item2 = $('.content-image-2 img');
	$('.image-item-2').on('mousemove',function(e) {  
		e.preventDefault();
		var ax3 = -($(window).innerWidth()/2- e.pageX)/400;
		var ay3 = ($(window).innerHeight()/2- e.pageY)/400;
		item2.attr("style", "transform: rotateY("+ax3+"deg) rotateX("+ay3+"deg);-webkit-transform: rotateY("+ax3+"deg) rotateX("+ay3+"deg);-moz-transform: rotateY("+ax3+"deg) rotateX("+ay3+"deg)");
	});
	$('.image-item-2').on('mouseout', function(e){
		item2.attr("style", "transition: .2s ease; transform: rotateY("+0+"deg) rotateX("+0+"deg);-webkit-transform: rotateY("+0+"deg) rotateX("+0+"deg);-moz-transform: rotateY("+0+"deg) rotateX("+0+"deg)");

	});

	var item3 = $('.content-image-3 img');
	$('.image-item-3').on('mousemove',function(e) {
		e.preventDefault();  
		var ax4 = -($(window).innerWidth()/2- e.pageX)/400;
		var ay4 = ($(window).innerHeight()/2- e.pageY)/400;
		item3.attr("style", "transform: rotateY("+ax4+"deg) rotateX("+ay4+"deg);-webkit-transform: rotateY("+ax4+"deg) rotateX("+ay4+"deg);-moz-transform: rotateY("+ax4+"deg) rotateX("+ay4+"deg)");
	});
	$('.image-item-3').on('mouseout', function(e){
		item3.attr("style", "transition: .2s ease; transform: rotateY("+0+"deg) rotateX("+0+"deg);-webkit-transform: rotateY("+0+"deg) rotateX("+0+"deg);-moz-transform: rotateY("+0+"deg) rotateX("+0+"deg)");

	});

	var item4 = $('.content-image-4 img');
	$('.image-item-4').on('mousemove',function(e) { 
		e.preventDefault(); 
		var ax5 = -($(window).innerWidth()/2- e.pageX)/400;
		var ay5 = ($(window).innerHeight()/2- e.pageY)/400;
		item4.attr("style", "transform: rotateY("+ax5+"deg) rotateX("+ay5+"deg);-webkit-transform: rotateY("+ax5+"deg) rotateX("+ay5+"deg);-moz-transform: rotateY("+ax5+"deg) rotateX("+ay5+"deg)");
	});
	$('.image-item-4').on('mouseout', function(e){
		item4.attr("style", "transition: .2s ease; transform: rotateY("+0+"deg) rotateX("+0+"deg);-webkit-transform: rotateY("+0+"deg) rotateX("+0+"deg);-moz-transform: rotateY("+0+"deg) rotateX("+0+"deg)");

	});


	var item5 = $('.content-image-5 img');
	$('.image-item-5').on('mousemove',function(e) {
		e.preventDefault();  
		var ax6 = -($(window).innerWidth()/2- e.pageX)/400;
		var ay6 = ($(window).innerHeight()/2- e.pageY)/400;
		item5.attr("style", "transform: rotateY("+ax6+"deg) rotateX("+ay6+"deg);-webkit-transform: rotateY("+ax6+"deg) rotateX("+ay6+"deg);-moz-transform: rotateY("+ax6+"deg) rotateX("+ay6+"deg)");
	});
	$('.image-item-5').on('mouseout', function(e){
		item5.attr("style", "transition: .2s ease; transform: rotateY("+0+"deg) rotateX("+0+"deg);-webkit-transform: rotateY("+0+"deg) rotateX("+0+"deg);-moz-transform: rotateY("+0+"deg) rotateX("+0+"deg)");

	});

}


function menuFixed(){
	
	var header = document.getElementById("header");
	var sticky = header.offsetTop;

	if( window.pageYOffset > sticky){
		header.classList.add('sticky');
		$('.logo-menu').removeClass('menu-init');
		$('.logo-menu').addClass('menu-fixed');
	} else {
		header.classList.remove('sticky');
		$('.logo-menu').removeClass('menu-fixed');
		$('.logo-menu').addClass('menu-init');
	}

}

// scroll
// function scrollBar(){

// 	var element =  document.querySelector('#scrollbar-wrapper');
// 	var options = {
// 		damping: 0.25,
// 		thumbMinSize: 5,
// 		renderByPixel: true,
// 		alwaysShowTracks: false,
// 		continuousScrolling: false
// 	};

// 	Scrollbar.init(element, options);
// 	document.querySelector('body').setAttribute('scroller', true);

// 	console.log(Scrollbar.has(element));

// }

function putMasks(){
	$('input[type="phone"]').mask('(00) 0 0000-0000');
}
