</scrollbar>
<footer>
	<div class="container pt-4 pb-4">
		<div class="row">
			<div class="col-12 col-lg-4 text-center text-lg-left">
				<a href="<?php echo get_site_url(); ?>">
					<img class="img-fluid" src="<?php echo get_template_directory_uri() . '/dist/images/logo-tipo.png'; ?>" alt="">
				</a>
				<p class="pt-4">A Tipo é uma empresa com anos de experiência no mercado de Marketing Digital, onde somos especializados nas mais diversas ferramentas de publicidade para atendermos nossos clientes da melhor forma possível.</p>
			</div>
			<div class="col-12 col-lg-4 text-center text-lg-left">
				<p class="text-white">Contato</p>


				<p><i class="fas fa-map-marker-alt"></i>Rua Málaga, 21 - Parque Sevilha </p>

				<p>	<i class="fas fa-phone"></i><a class="text-white" href="">(11) 9 9999-9999</a></p>

				<p>	<i class="fas fa-envelope"></i><a href="" class="text-white" >teste@teste.com.br</a></p>
			</div>
			<div class="col-12 col-lg-4 d-none d-lg-block">
				<p class="text-white">Entre em contato</p>

				<form class="form-footer" method="post">
					<div class="form-group">	
						<input type="text" class="form-control" name="Nome" placeholder="Nome" required>
					</div>

					<div class="form-group">	
						<input type="mail" class="form-control" name="E-mail" placeholder="E-mail" required>
					</div>

					<div class="form-group">	
						<input type="phone" class="form-control" name="Telefone" placeholder="Telefone" pattern=".{15,16}" required title="Insira um número de celular ou telefone.">
					</div>

					<button class="btn-submit">Enviar</button>

				</form>

			</div>
			<div class="col-12 col-lg-6 pt-3 pb-3 p-lg-0 text-center text-lg-left footer-bottom">
				<a target="_blank" href="https://pt-br.facebook.com/tipopublicidade/"><i class="fab fa-facebook-f"></i></a>
				<a target="_blank" href="https://www.instagram.com/tipopublicidade/?hl=pt-br"><i class="fab fa-instagram"></i></a>
				<a target="_blank" href="https://www.linkedin.com/company/tipopublicidade/"><i class="fab fa-linkedin"></i></a>
			</div>
			<div class="col-12 col-lg-6 text-center text-lg-right">
				
			</div>
		</div>
	</div>

	
</footer>

<?php 
wp_footer();
?>



</div>
</body>
</html> 
