// npm uninstall `ls -1 node_modules | tr '/\n' ' '`

var gulp = require('gulp'),
stylus = require('gulp-stylus'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
postcss = require('gulp-postcss'),
sourcemaps = require('gulp-sourcemaps'),
packer = require('css-mqpacker'),
prefixes = require('autoprefixer'),
cssnano = require('cssnano'),
pump = require('pump');

gulp.task('stylus', function () {
    var stylesheets = [
       'src/stylus/*.styl',
    ];
    
    return gulp.src(stylesheets)
    .pipe(stylus())
    .pipe(gulp.dest('src'));
    });

gulp.task('css', function () {
    var processors = [
    prefixes({
        browsers: ['last 3 versions']
        }),
    packer(),
    cssnano({zindex: false,reduceIdents: false}),
    ];
    return gulp.src('src/*.css')
    .pipe(sourcemaps.init())
    .pipe(postcss(processors))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist'));
    });

gulp.task('compress', function (done) {

    var scripts = [
        'node_modules/sweetalert2/dist/sweetalert2.all.js', // sweet alert 2
        'node_modules/smooth-scrollbar/dist/smooth-scrollbar.js', // smooth scrollbar
        'src/js/*.js'
        ];
        gulp.src(scripts)
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
        done();
        });

gulp.task('uglify-debug', function (cb) {
    pump([
        gulp.src('src/js/*.js'),
        concat('app.js'),
        uglify(),
        gulp.dest('dist')
        ], cb);
    });

gulp.task('watch', function (done) {
    gulp.watch('src/stylus/**/*.styl', gulp.series('stylus', 'css'));
    gulp.watch('src/js/**/*.js', gulp.series('compress'));
    done;
    });

gulp.task('default', gulp.series('stylus', 'css', 'compress', 'watch'));