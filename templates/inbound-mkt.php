<?php
/*
Template Name: Página Inbound Marketing

*/
get_header();
?>
<section id="page-inbound">
    <div class="container">
        <div class="row">
            <div class="content w-100">

                <div class="col-12 col-lg-12 w-100 text-center">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/dist/images/inbound/home-inbound.png'; ?>" alt="">
                </div>
                
                <div class="col-12 col-lg-12 text-center pt-lg-5 pt-0">
                    <h2 class="sub-title pb-4" ><strong>Atrair, converter, relacionar e vender</strong></h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam esse, corporis laboriosam magnam voluptas ipsum reiciendis, deserunt accusamus sit iusto pariatur repellat sunt dolore quidem sapiente facere officia velit fugit.</p>
                </div>

                <div class="atracao pt-5 pt-lg-0">
                    <div class="row">
                    <div class="col-6 col-lg-7">
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/dist/images/inbound/atracao.png'; ?>" alt="">
                    </div>

                    <div class="col-6 col-lg-5 text-aside">
                        <h3><strong>Atração</strong></h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates quas a at? Fuga, ratione recusandae fugiat totam molestiae doloremque nisi harum et quod, laboriosam itaque enim provident praesentium. Odit, error!               
                        </p>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequatur at repellat laboriosam ab ut et, magni doloremque rem ex enim ipsum culpa, necessitatibus sequi repellendus vitae ducimus sunt, quidem repudiandae.</p>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequatur at repellat laboriosam ab ut et, magni doloremque rem ex enim ipsum culpa, necessitatibus sequi repellendus vitae ducimus sunt, quidem repudiandae.</p>
                        
                        <h4 class="text-center pt-3"><strong>Marketing de conteúdo</strong></h4>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus dignissimos aut impedit neque ullam beatae saepe rerum! Sed saepe, porro recusandae veniam placeat dolore quia rerum nam, cumque sit iure.</p>                        
                    </div>

                    </div>
                </div>

                <div class="converter pt-5">
                    <div class="row">
                        <div class="col-6 col-lg-5 text-aside">
                            <h3><strong>Converter</strong></h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis nemo alias doloremque incidunt quos, optio tenetur corrupti vitae quisquam porro. Corrupti aliquam iusto qui facere, dolore necessitatibus quia dignissimos voluptatem.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt velit animi at officiis excepturi. Ab quas dolore consequuntur, id hic accusantium adipisci, tenetur quae ut, a nihil eos! Eius, veniam.</p>
                            <h4 class="text-center pt-3"><strong>Materiais ricos</strong></h4>
                        <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus dignissimos aut impedit neque ullam beatae saepe rerum! Sed saepe, porro recusandae veniam placeat dolore quia rerum nam, cumque sit iure.</p>                        
                    </div>
                    <div class="col-6 col-lg-7">
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/dist/images/inbound/conversao.png'; ?>" alt="">
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





<?php get_footer(); ?>