<?php
/*
	Template Name: Página Porque a Tipo
*/
	get_header();
	$img = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumb-foto');
	?>
	<section id="breadcrumb">
		<img class="img-breadcrumb" src="<?php echo $img; ?>" alt="<?php the_title(); ?>"
		loading="lazy">
		<div class="container mt-4">
			<div class="col-12 text-center">
				<h1 class="mb-4"><?php the_title(); ?></h1>
				<?php the_breadcrumb(); ?>
			</div>
		</div>
	</section>

	<section id="content-single">
		<div class="container pt-5 pb-5">
			<div class="row">
				<div class="col-12 col-lg-2 pb-5">
					<p class="about-title">
						Sobre a Tipo Publicidade
					</p>
				</div>
				<div class="col-12 col-lg-10 pb-5">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus modi, ipsa ratione eius et placeat minus eos vel veritatis, temporibus, voluptates ducimus commodi doloremque adipisci dolor omnis rerum vero explicabo! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eum numquam explicabo ad natus libero enim cupiditate omnis vitae placeat esse eius, similique excepturi, assumenda tenetur dolorem illum! Ex, incidunt.
					</p>
				</div>


				<div class="col-12 col-lg-4">
					<h4>Lorem ipsum dolor sit amet</h4>
					<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas nulla maiores nobis blanditiis quisquam voluptatibus dolores in harum vero nisi, autem rerum nesciunt, illum distinctio ipsam dolore molestias totam, explicabo.</p>
				</div>
				<div class="col-12 col-lg-4">
					<h4>Lorem ipsum dolor sit amet</h4>
					<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas nulla maiores nobis blanditiis quisquam voluptatibus dolores in harum vero nisi, autem rerum nesciunt, illum distinctio ipsam dolore molestias totam, explicabo.</p>
				</div>
				<div class="col-12 col-lg-4">
					<h4>Lorem ipsum dolor sit amet</h4>
					<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas nulla maiores nobis blanditiis quisquam voluptatibus dolores in harum vero nisi, autem rerum nesciunt, illum distinctio ipsam dolore molestias totam, explicabo.</p>
				</div>
				
			</div>
		</div>
	</section>
	<section id="items-full">
		<div class="row">
			<div class="col-12 col-lg-4 item-count">
				<h3>Título</h3>
				<h4>Subtitulo</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus modi, ipsa ratione eius et placeat minus eos vel veritatis, temporibus, voluptates ducimus commodi doloremque adipisci dolor omnis rerum vero explicabo! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eum numquam explicabo ad natus libero enim cupiditate omnis vitae placeat esse eius, similique excepturi, assumenda tenetur dolorem illum! Ex, incidunt.
				</p>
			</div>
				<div class="col-12 col-lg-4 item-count">
					<h3>Título</h3>
					<h4>Subtitulo</h4>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus modi, ipsa ratione eius et placeat minus eos vel veritatis, temporibus, voluptates ducimus commodi doloremque adipisci dolor omnis rerum vero explicabo! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eum numquam explicabo ad natus libero enim cupiditate omnis vitae placeat esse eius, similique excepturi, assumenda tenetur dolorem illum! Ex, incidunt.
					</p>
				</div>
				<div class="col-12 col-lg-4 item-count">
					<h3>Título</h3>
					<h4>Subtitulo</h4>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus modi, ipsa ratione eius et placeat minus eos vel veritatis, temporibus, voluptates ducimus commodi doloremque adipisci dolor omnis rerum vero explicabo! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eum numquam explicabo ad natus libero enim cupiditate omnis vitae placeat esse eius, similique excepturi, assumenda tenetur dolorem illum! Ex, incidunt.
					</p>
				</div>
			</div>
		</section>

		<section id="team">	
			<div class="container pb-4">	
				<div class="row">	
					<div class="col-12 text-center pt-3 pb-4">	
						<h4 class="pb-3">Equipe</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit accusamus, deserunt, aut labore facere praesentium magni eum nemo possimus ipsa corporis blanditiis rem eligendi eius amet consequatur est ut perspiciatis!</p>
					</div>

					<div class="col-12 col-lg-3 text-center pb-4">
						<img class="pb-3" src="http://placekitten.com/380/300" alt="">
						<p>Nome</p>
						<p>Cargo</p>
					</div>
					<div class="col-12 col-lg-3 text-center pb-4">
						<img class="pb-3" src="http://placekitten.com/380/300" alt="">
						<p>Nome</p>
						<p>Cargo</p>
					</div>
					<div class="col-12 col-lg-3 text-center pb-4">
						<img class="pb-3" src="http://placekitten.com/380/300" alt="">
						<p>Nome</p>
						<p>Cargo</p>
					</div>
					<div class="col-12 col-lg-3 text-center pb-4">
						<img class="pb-3" src="http://placekitten.com/380/300" alt="">
						<p>Nome</p>
						<p>Cargo</p>
					</div>


				</div>
			</div>

		</section>




		<?php get_footer(); ?>