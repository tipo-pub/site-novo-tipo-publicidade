<?php
/*
	Template Name: Página Cases
*/
	get_header();
	$img = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumb-foto');
	?>
	<section id="breadcrumb">
		<img class="img-breadcrumb" src="<?php echo $img; ?>" alt="<?php the_title(); ?>"
		loading="lazy">
		<div class="container mt-4">
			<div class="col-12 text-center">
				<h1 class="mb-4"><?php the_title(); ?></h1>
				<?php  the_breadcrumb(); ?>
			</div>
		</div>
	</section>
	<section id="content-case">	
		<div class="container mb-4 pt-5">	
			<div class="row">

				<div class="col-12 col-lg-3">
					<div class="col-12">
						<div class="list">
							<ul>
								<li> <i class="fas fa-dot-circle"></i><a href="#service-1">Service 1</a> </li>
								<li> <i class="fas fa-dot-circle"></i><a href="#service-2">Service 2</a> </li>
								<li> <i class="fas fa-dot-circle"></i><a href="#service-3">Service 3</a> </li>
								<li> <i class="fas fa-dot-circle"></i><a href="#service-4">Service 4</a> </li>
								<li> <i class="fas fa-dot-circle"></i><a href="#service-5">Service 5</a> </li>
							</ul>
							

						</div>
					</div>
				</div>

				<div class="col-12 col-lg-9">
					<section id="service-1">
						<h3>Section</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>


					</section>

					<section id="service-2">
						<h3>Section</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>


					</section>
					<section id="service-3">
						<h3>Section</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

					</section>
					<section id="service-4">
						<h3>Section</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>


					</section>
					<section id="service-5">
						<h3>Section</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam ut molestias dolor nam natus ipsam, accusantium eveniet quae quis nesciunt, consequuntur eos, fuga itaque, maxime minus. Illo asperiores accusantium, itaque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat harum fugit ratione voluptatum illo, totam odit atque voluptate cumque nulla assumenda doloribus hic, eos qui impedit architecto dolores maxime tempora.</p>


					</section>

				</div>

			</div>
		</div>


	</section>



	<?php get_footer(); ?>