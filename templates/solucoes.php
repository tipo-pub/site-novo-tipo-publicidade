<?php
/*
	Template Name: Página Soluções
*/
	get_header();
	$img = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumb-foto');
	?>
	<section id="breadcrumb">
		<img class="img-breadcrumb" src="<?php echo $img; ?>" alt="<?php the_title(); ?>"
		loading="lazy">
		<div class="container mt-4">
			<div class="col-12 text-center">
				<h1 class="mb-4"><?php the_title(); ?></h1>
				<?php  the_breadcrumb(); ?>
			</div>
		</div>
	</section>
	<section id="content-single">	
		<div class="container mb-4 pt-5">	
			<div class="row flex-column-reverse flex-md-row pt-4">	
				
			
					<div class="col-12 col-lg-6 text-center content-item pb-3">
						<div class="image-item-1">
							<div class="content-image-1">
								<img src="<?php echo get_template_directory_uri(). '/dist/images/solutions/phone.png';?>" alt="">
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-6 content-item text-lg-left">
						<div class="content-text">
							<h2>Mídia Social</h2>
							<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates, et numquam hic fuga earum amet quasi atque non animi illo rerum ratione voluptatem commodi qui tenetur nisi perferendis velit?</p>
							<button class="btn-default mt-4 mb-4">
								<a href="#">Saiba Mais</a>
							</button>
						</div>
					</div>
				

				
					<div class="col-12 col-lg-6 content-item text-lg-right">
						<div class="content-text">
							<h2>Mídia Social</h2>
							<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates, et numquam hic fuga earum amet quasi atque non animi illo rerum ratione voluptatem commodi qui tenetur nisi perferendis velit?</p>
							<button class="btn-default mt-4 mb-4">
								<a href="#">Saiba Mais</a>
							</button>
						</div>
					</div>
					<div class="col-12 col-lg-6 text-center content-item pb-3">
						<div class="image-item-2">
							<div class="content-image-2">
								<img src="<?php echo get_template_directory_uri(). '/dist/images/solutions/phone.png';?>" alt="">
							</div>
						</div>
					</div>
				


				
					<div class="col-12 col-lg-6 text-center content-item pb-3">
						<div class="image-item-3">
							<div class="content-image-3">
								<img src="<?php echo get_template_directory_uri(). '/dist/images/solutions/phone.png';?>" alt="">
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-6 content-item text-lg-left">
						<div class="content-text">
							<h2>Mídia Social</h2>
							<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates, et numquam hic fuga earum amet quasi atque non animi illo rerum ratione voluptatem commodi qui tenetur nisi perferendis velit?</p>
							<button class="btn-default mt-4 mb-4">
								<a href="#">Saiba Mais</a>
							</button>
						</div>
					</div>
				


				
					
					<div class="col-12 col-lg-6 content-item text-lg-right">
						<div class="content-text">
							<h2>Mídia Social</h2>
							<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates, et numquam hic fuga earum amet quasi atque non animi illo rerum ratione voluptatem commodi qui tenetur nisi perferendis velit?</p>
							<button class="btn-default mt-4 mb-4">
								<a href="#">Saiba Mais</a>
							</button>
						</div>
					</div>

					<div class="col-12 col-lg-6 text-center content-item pb-3">
						<div class="image-item-4">
							<div class="content-image-4">
								<img src="<?php echo get_template_directory_uri(). '/dist/images/solutions/phone.png';?>" alt="">
							</div>
						</div>
					</div>



				
					<div class="col-12 col-lg-6 text-center content-item pb-3">
						<div class="image-item-5">
							<div class="content-image-5">
								<img src="<?php echo get_template_directory_uri(). '/dist/images/solutions/phone.png';?>" alt="">
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-6 content-item text-lg-left">
						<div class="content-text">
							<h2>Mídia Social</h2>
							<p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates, et numquam hic fuga earum amet quasi atque non animi illo rerum ratione voluptatem commodi qui tenetur nisi perferendis velit?</p>
							<button class="btn-default mt-4 mb-4">
								<a href="#">Saiba Mais</a>
							</button>
						</div>
					</div>
				





			</div>
		</div>


	</section>



	<?php get_footer(); ?>