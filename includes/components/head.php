<head>
    <meta charset="UTF-8">
    <title>
        <?php echo wp_title(); ?>
    </title>
    <meta name="description" content="A Tipo Publicidade é uma empresa de Marketing Digital, especializada nas mais diversas ferramentas de publicidade para atender seus clientes da melhor forma possível. Acesse o site, confira os métodos inovadores que a empresa oferece, e faça seu orçamento agora mesmo!">
    <meta name="language" content="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="Tipo Publicidade">
    <meta name="language" content="pt-br" />
    <link rel="canonical" href="<?= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>" />
    <?php wp_head(); ?>
</head>

<header>
    <div id="header" class="desktop-menu d-none d-lg-block">  
        <div class="container">
            <div class="row outline-menu">
                <div class="col-lg-4 p-2 pt-2">
                    <a class="logo-menu" data-toggle="tooltip" data-placement="top" title="Home" href="<?php echo get_site_url(); ?>">
                        
                    </a>
                </div>
                <div class="col-lg-8 p-2 pt-3 text-right">
                    <div class="menu-top">
                        <?php 
                        wp_nav_menu([
                            'theme_location' => 'main-menu',
                            'menu_class' => 'menu-top',
                            'container-class' => 'menu-top',
                        ]);
                        ?>
                    </div>   
                </div>

            </div>
        </div>
    </div>
    <div class="mobile-menu pb-3 d-block d-lg-none"> 
        <div class="container-fluid">   
            <div class="row">   
                <div class="col-6 pt-3"> 
                  <a href="<?php home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri() . '/dist/images/logo-tipo.png'; ?>" alt="">
                </a>
            </div>
            <div class="col-6 pt-3 text-right"> 
                <button class="btn-menu">
                    <i class="fas fa-bars open"></i>
                    <i class="fas fa-times close-menu"></i>
                </button>
            </div>
            <div class="menu-opened">
                <div class="col-12 text-center">
                   <a class="logo-menu" href="<?php home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri() . '/dist/images/logo-tipo.png'; ?>" alt="">
                </a>
            </div>

            <div class="col-12">
                <?php 
                wp_nav_menu([
                    'theme_location' => 'main-menu-mobile',
                    'menu_class' => 'options-mobile',
                    'container-class' => 'options-mobile',
                ]);
                ?>  
            </div>
            <div class="social-mobile d-flex">
                <div class="col-4 text-center">
                   <a href="https://pt-br.facebook.com/tipopublicidade/"><i class="fab fa-facebook-f"></i></a> 
               </div>
               <div class="col-4 text-center">
                <a href="https://www.instagram.com/tipopublicidade/?hl=pt-br"><i class="fab fa-instagram"></i></a>
            </div>
            <div class="col-4 text-center">
                <a href="https://www.linkedin.com/company/tipopublicidade/"><i class="fab fa-linkedin"></i></a>
            </div>
        </div>

    </div>

</div>        
</div>
</div>
</header>